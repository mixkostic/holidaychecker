package com.example.holidaychecker

import com.example.holidaychecker.models.holiday.Date
import com.example.holidaychecker.models.holiday.Holiday
import com.example.holidaychecker.models.holiday.Observed
import com.example.holidaychecker.models.holiday.Weekday
import org.junit.Before
import org.junit.Test

class HolidayCheckerTest {
    private val holidayChecker = HolidayChecker()
    private var holidayListCountryA: List<Holiday> = emptyList()
    private var holidayListCountryB: List<Holiday> = emptyList()
    private var commonPublicHolidaysCountryA: List<Holiday> = emptyList()
    private var distinctHolidaysCountryA: List<Holiday> = emptyList()

    private fun initHolidaysCountryA() {
        val holiday1 = Holiday(country="GB", date="2018-01-01", name="New Year's Day",
            observed="2018-01-01", publicHoliday= true, uuid="2e80d690-7510-44ed-b8f9-486c7ee1d25b",
            weekday=Weekday(date= Date(name="Monday", numeric="1"),
                observed= Observed(name="Monday", numeric="1")))

        val holiday2 = Holiday(country="GB", date="2018-03-30", name="Good Friday",
            observed="2018-03-30", publicHoliday=true, uuid="b587ab15-2440-42dd-a861-43fbecbc8432",
            weekday=Weekday(date=Date(name="Friday", numeric="5"),
                observed=Observed(name="Friday", numeric="5")))

        val holiday3 = Holiday(country="GB", date="2018-12-25", name="Christmas Day",
            observed="2018-12-25", publicHoliday=true, uuid="41eb5678-2ada-4d0e-ae60-65595afb476c",
            weekday=Weekday(date=Date(name="Tuesday", numeric="2"),
                observed=Observed(name="Tuesday", numeric="2")))

        val holiday4 = Holiday(country="GB", date="2018-12-26", name="Boxing Day",
            observed="2018-12-26", publicHoliday=true, uuid="b3f105ee-284e-497c-8d62-5199b48938c9",
            weekday=Weekday(date=Date(name="Wednesday", numeric="3"),
                observed=Observed(name="Wednesday", numeric="3")))

        commonPublicHolidaysCountryA = arrayListOf(holiday1, holiday3)
        distinctHolidaysCountryA = arrayListOf(holiday2, holiday4)

        holidayListCountryA = arrayListOf(holiday1, holiday2, holiday3, holiday4)

    }

    private fun initHolidaysCountryB() {
        val holiday1 = Holiday(country="US", date="2018-01-01", name="New Year's Day",
            observed="2018-01-01", publicHoliday=true, uuid="82f78b8a-019e-479e-a19f-99040275f9bf",
            weekday=Weekday(date=Date(name="Monday", numeric="1"),
                observed=Observed(name="Monday", numeric="1")))

        val holiday2 = Holiday(country="US", date="2018-01-07", name="Orthodox Christmas Day",
            observed="2018-01-07", publicHoliday=false, uuid="8ed029b2-c39d-46aa-b15a-4c67540380e4",
            weekday=Weekday(date=Date(name="Sunday", numeric="7"),
                observed=Observed(name="Sunday", numeric="7")))

        val holiday3 = Holiday(country="US", date="2018-01-14", name="Orthodox New Year's Day",
            observed="2018-01-14", publicHoliday=false, uuid="4f7a5c31-18fa-4ffc-a47c-b889576ca002",
            weekday=Weekday(date=Date(name="Sunday", numeric="7"),
                observed=Observed(name="Sunday", numeric="7")))

        val holiday4 = Holiday(country="US", date="2018-12-25", name="Christmas Day",
            observed="2018-12-25", publicHoliday=true, uuid="57787cbd-300c-48e4-a456-165c8bdc97b0",
            weekday=Weekday(date=Date(name="Tuesday", numeric="2"),
                observed=Observed(name="Tuesday", numeric="2")))

        holidayListCountryB = arrayListOf(holiday1, holiday2, holiday3, holiday4)
    }

    private fun getColapsingList(): List<Holiday> {
        val holiday1 = Holiday(country="AE", date="2018-06-12", name="Festival of Breaking the Fast",
            observed="2018-06-12", publicHoliday=true, uuid="3a227a32-9760-4e34-bea6-37862cf8c53f",
            weekday=Weekday(date=Date(name="Tuesday", numeric="2"),
                observed=Observed(name="Tuesday", numeric="2")))

        val holiday2 = Holiday(country="AE", date="2018-06-13", name="Second Day of the Festival of Breaking the Fast",
            observed="2018-06-13", publicHoliday=true, uuid="5ada2069-b001-40e8-8056-d7da0014f55a",
            weekday=Weekday(date=Date(name="Wednesday", numeric="3"),
                observed=Observed(name="Wednesday", numeric="3")))

        val holiday3 = Holiday(country="AE", date="2018-06-14", name="Third Day of the Festival of Breaking the Fast",
            observed="2018-06-14", publicHoliday=true, uuid="0971d510-77e7-480e-8667-19c80042a6d0",
            weekday=Weekday(date=Date(name="Thursday", numeric="4"),
                observed=Observed(name="Thursday", numeric="4")))

        val holiday4 = Holiday(country="AE", date="2018-08-20", name="Day of Arafah",
            observed="2018-08-20", publicHoliday=true, uuid="08e5b8f8-3756-46a9-914a-9dfc023025ad",
            weekday=Weekday(date=Date(name="Monday", numeric="1"),
                observed=Observed(name="Monday", numeric="1")))

        return arrayListOf(holiday1, holiday2, holiday3, holiday4)
    }

    @Before
    fun setup() {
        initHolidaysCountryA()
        initHolidaysCountryB()
    }


    @Test
    fun testCommonPublicHolidays() {
        val commonPublicHolidays = holidayChecker.commonPublicHolidays(holidayListCountryA, holidayListCountryB)

        val condition = commonPublicHolidays.isNotEmpty()
                && commonPublicHolidays.size == commonPublicHolidaysCountryA.size
                && commonPublicHolidays.all { it.publicHoliday }
                && commonPublicHolidays.containsAll(commonPublicHolidaysCountryA)

        assert(condition)
    }

    @Test
    fun testCommonPublicHolidays_emptyList() {
        val commonPublicHolidays = holidayChecker.commonPublicHolidays(emptyList(), holidayListCountryB)

        assert(commonPublicHolidays.isEmpty())
    }

    @Test
    fun testCommonPublicHolidays_emptyList2() {
        val commonPublicHolidays = holidayChecker.commonPublicHolidays(holidayListCountryB, emptyList())

        assert(commonPublicHolidays.isEmpty())
    }

    @Test
    fun testDistinct() {
        val distinct = holidayChecker.distinctHolidays(holidayListCountryA, holidayListCountryB)

        val condition = distinct.isNotEmpty()
                && distinct.size == distinctHolidaysCountryA.size
                && distinct.containsAll(distinctHolidaysCountryA)

        assert(condition)
    }

    @Test
    fun testDistinct_emptyList() {
        val distinct = holidayChecker.distinctHolidays(holidayListCountryA, emptyList())

        val condition = distinct.isNotEmpty()
                && distinct.size == holidayListCountryA.size
                && distinct.containsAll(holidayListCountryA)

        assert(condition)
    }

    @Test
    fun testDistinct_emptyList2() {
        val distinct = holidayChecker.distinctHolidays(emptyList(), holidayListCountryB)

        assert(distinct.isEmpty())
    }

    @Test
    fun testCollapse() {
        val list = holidayChecker.collapseContiguousHolidays(getColapsingList())

        assert(list.size == 2)
    }

    @Test
    fun testCollapse_emptyList() {
        val list = holidayChecker.collapseContiguousHolidays(emptyList())

        assert(list.isEmpty())
    }

    @Test
    fun testCollapse_ParseExceptionNotThrown() {
        val holidayWithBadDate = Holiday(country="GB", date="2018-bad-date", name="Good Friday",
            observed="2018-03-30", publicHoliday=true, uuid="b587ab15-2440-42dd-a861-43fbecbc8432",
            weekday=Weekday(date=Date(name="Friday", numeric="5"),
                observed=Observed(name="Friday", numeric="5")))

        val holidayWithRegularDate = Holiday(country="GB", date="2018-01-01", name="New Year's Day",
            observed="2018-01-01", publicHoliday= true, uuid="2e80d690-7510-44ed-b8f9-486c7ee1d25b",
            weekday=Weekday(date= Date(name="Monday", numeric="1"),
                observed= Observed(name="Monday", numeric="1")))

        holidayChecker.collapseContiguousHolidays(listOf(holidayWithRegularDate, holidayWithBadDate))
    }

}