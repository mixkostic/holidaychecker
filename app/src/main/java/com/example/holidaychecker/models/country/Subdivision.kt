package com.example.holidaychecker.models.country

data class Subdivision(
    val code: String,
    val languages: List<String>,
    val name: String
)