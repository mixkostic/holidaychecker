package com.example.holidaychecker.models.holiday

import com.example.holidaychecker.models.Requests

data class HolidaysData(
    val holidays: List<Holiday>,
    val requests: Requests,
    val status: Int
)