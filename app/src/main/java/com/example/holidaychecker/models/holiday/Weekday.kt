package com.example.holidaychecker.models.holiday

data class Weekday(
    val date: Date,
    val observed: Observed
)