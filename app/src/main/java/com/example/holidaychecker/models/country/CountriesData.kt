package com.example.holidaychecker.models.country

import com.example.holidaychecker.models.Requests

data class CountriesData(
    val countries: List<Country>,
    val requests: Requests,
    val status: Int
)