package com.example.holidaychecker.models

data class Requests(
    val available: Int,
    val resets: String,
    val used: Int
)