package com.example.holidaychecker.models.holiday

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Holiday(
    var country: String = "",
    var date: String = "",
    var name: String = "",
    var observed: String = "",
    @ColumnInfo(name = "public") @SerializedName("public") var publicHoliday: Boolean = false,
    @PrimaryKey var uuid: String = "",
    @Ignore var weekday: Weekday = Weekday(Date("", ""), Observed("", ""))
)