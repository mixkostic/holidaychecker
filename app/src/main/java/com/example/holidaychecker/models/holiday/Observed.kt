package com.example.holidaychecker.models.holiday

data class Observed(
    val name: String,
    val numeric: String
)