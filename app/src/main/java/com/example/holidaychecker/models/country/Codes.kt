package com.example.holidaychecker.models.country

import com.google.gson.annotations.SerializedName

data class Codes(
    @SerializedName("alpha-2") val alpha2: String,
    @SerializedName("alpha-3") val alpha3: String,
    val numeric: String
)