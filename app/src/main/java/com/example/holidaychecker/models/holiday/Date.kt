package com.example.holidaychecker.models.holiday

data class Date(
    val name: String,
    val numeric: String
)