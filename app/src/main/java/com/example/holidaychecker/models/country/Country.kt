package com.example.holidaychecker.models.country

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "country")
data class Country(
    @PrimaryKey @ColumnInfo(name = "code") var code: String = "",
    @Ignore var codes: Codes = Codes("", "", ""),
    @ColumnInfo(name = "flag") var flag: String = "",
    @Ignore var languages: List<String> = emptyList(),
    @ColumnInfo(name = "name") var name: String = "",
    @Ignore var subdivisions: List<Subdivision> = emptyList()
)