package com.example.holidaychecker.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.holidaychecker.models.holiday.Holiday
import com.example.holidaychecker.repository.HolidaysRepository
import com.example.holidaychecker.room.HolidayCheckerDatabase

class HolidaysViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: HolidaysRepository

    init {
        val holidaysDao = HolidayCheckerDatabase.getDatabase(application).holidayDao()
        repository = HolidaysRepository(holidaysDao)
    }

    fun getHolidays(country: String): LiveData<List<Holiday>> {
        return repository.getHolidays(country)
    }
}