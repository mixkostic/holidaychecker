package com.example.holidaychecker.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.holidaychecker.room.HolidayCheckerDatabase
import com.example.holidaychecker.models.country.Country
import com.example.holidaychecker.repository.CountriesRepository

class CountriesViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: CountriesRepository
    private var countryLiveData: LiveData<List<Country>>? = null

    init {
        val countriesDao = HolidayCheckerDatabase.getDatabase(application).countryDao()
        repository = CountriesRepository(countriesDao)
    }

    fun getCountriesList(): LiveData<List<Country>> {
        if (countryLiveData == null) {
            countryLiveData = repository.getCountries()
        }
        return countryLiveData!!
    }
}