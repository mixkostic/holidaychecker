package com.example.holidaychecker.activity

import android.graphics.Rect
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.holidaychecker.HolidayAction
import com.example.holidaychecker.HolidayChecker
import com.example.holidaychecker.R
import com.example.holidaychecker.adapter.HolidayAdapter
import com.example.holidaychecker.models.holiday.Holiday
import com.example.holidaychecker.viewmodel.HolidaysViewModel

class HolidaysActivity : AppCompatActivity() {
    companion object {
        const val COUNTRY_A = "countryA"
        const val COUNTRY_B = "countryB"
        const val HOLIDAY_ACTION = "holiday_action"
        const val COLLAPSE_ACTION = "collapse_action"
        private const val recyclerViewItemPadding = 50
    }

    private var holidaysCountryA: List<Holiday>? = null
    private var holidaysCountryB: List<Holiday>? = null
    private val holidayChecker = HolidayChecker()
    private var holidayAction: String? = ""
    private var collapseHolidays = true

    private var recyclerView: RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_holidays)

        val intent = intent
        val countryA = intent.getStringExtra(COUNTRY_A)
        val countryB = intent.getStringExtra(COUNTRY_B)
        holidayAction = intent.getStringExtra(HOLIDAY_ACTION)
        collapseHolidays = intent.getBooleanExtra(COLLAPSE_ACTION, true)

        setupRecyclerViewLayout()

        val holidaysViewModel = ViewModelProviders.of(this).get(HolidaysViewModel::class.java)

        holidaysViewModel.getHolidays(countryA).observe(this, Observer<List<Holiday>> {
            holidaysCountryA = it
            initUI()
        })

        holidaysViewModel.getHolidays(countryB).observe(this, Observer<List<Holiday>> {
            holidaysCountryB = it
            initUI()
        })
    }

    private fun setupRecyclerViewLayout() {
        recyclerView = findViewById(R.id.recyclerView_holidays)
        recyclerView?.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerView?.addItemDecoration(MarginItemDecoration(recyclerViewItemPadding))
    }

    private fun initUI() {
        if (holidaysCountryA != null && holidaysCountryB != null) {
            var resultingHolidays: List<Holiday> = emptyList()
            when(holidayAction) {
                HolidayAction.COMMON_PUBLIC.action -> resultingHolidays = holidayChecker.commonPublicHolidays(holidaysCountryA!!, holidaysCountryB!!)
                HolidayAction.AminusB.action -> resultingHolidays = holidayChecker.distinctHolidays(holidaysCountryA!!, holidaysCountryB!!)
                HolidayAction.BminusA.action -> resultingHolidays = holidayChecker.distinctHolidays(holidaysCountryB!!, holidaysCountryA!!)
            }

            if (collapseHolidays) {
                resultingHolidays = holidayChecker.collapseContiguousHolidays(resultingHolidays)
            }

            setRecyclerViewAdapter(resultingHolidays)
        }
    }

    private fun setRecyclerViewAdapter(holidayList: List<Holiday>) {
        recyclerView?.adapter = HolidayAdapter(holidayList)
    }

    class MarginItemDecoration(private val spaceHeight: Int) : RecyclerView.ItemDecoration() {
        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
            with(outRect) {
                if (parent.getChildAdapterPosition(view) == 0) {
                    top = spaceHeight
                }
                left = spaceHeight
                right = spaceHeight
                bottom = spaceHeight
            }
        }
    }
}
