package com.example.holidaychecker.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.CheckBox
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.holidaychecker.HolidayAction
import com.example.holidaychecker.R
import com.example.holidaychecker.models.country.Country
import com.example.holidaychecker.viewmodel.CountriesViewModel

class CountriesActivity : AppCompatActivity(), View.OnClickListener {
    private var spinnerCountryA: Spinner? = null
    private var spinnerCountryB: Spinner? = null
    private var listOfCountries: List<Country> = emptyList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_countries)

        val countriesViewModel = ViewModelProviders.of(this).get(CountriesViewModel::class.java)
        countriesViewModel.getCountriesList().observe(this, Observer<List<Country>>{
            listOfCountries = it
            initUI(it)
        })

    }

    override fun onClick(p0: View?) {
        val action = when(p0?.id) {
            R.id.button_a_b -> HolidayAction.AminusB
            R.id.button_b_a -> HolidayAction.BminusA
            R.id.button_common -> HolidayAction.COMMON_PUBLIC
            else -> HolidayAction.COMMON_PUBLIC
        }

        openHolidaysActivity(getSelectedCountryCode(spinnerCountryA),
            getSelectedCountryCode(spinnerCountryB), action)

    }

    private fun initUI(countryList: List<Country>) {
        spinnerCountryA = findViewById(R.id.spinner_countryA)
        spinnerCountryB = findViewById(R.id.spinner_countryB)

        initSpinner(spinnerCountryA, countryList)
        initSpinner(spinnerCountryB, countryList)
        initButtons()
    }

    private fun initSpinner(spinner: Spinner?, countryList: List<Country>) {
        spinner?.let {
            val dataAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, countryList.map { it.name })
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = dataAdapter
        }

    }

    private fun initButtons() {
        val buttonCommon = findViewById<Button>(R.id.button_common)
        val buttonAminusB = findViewById<Button>(R.id.button_a_b)
        val buttonBminusA = findViewById<Button>(R.id.button_b_a)
        buttonCommon.setOnClickListener(this)
        buttonAminusB.setOnClickListener(this)
        buttonBminusA.setOnClickListener(this)
    }

    private fun getSelectedCountryCode(spinner: Spinner?): String {
        val selectedItemIndex = spinner?.selectedItemPosition ?: -1
        return if (selectedItemIndex != -1) listOfCountries[selectedItemIndex].code
                else ""
    }

    private fun openHolidaysActivity(countryA: String, countryB: String, action: HolidayAction) {
        val intent = Intent(this, HolidaysActivity::class.java)
        intent.putExtra(HolidaysActivity.COUNTRY_A, countryA)
        intent.putExtra(HolidaysActivity.COUNTRY_B, countryB)
        intent.putExtra(HolidaysActivity.HOLIDAY_ACTION, action.action)
        intent.putExtra(HolidaysActivity.COLLAPSE_ACTION, getCollapseState())
        startActivity(intent)
    }

    private fun getCollapseState(): Boolean {
        val checkBox = findViewById<CheckBox>(R.id.checkBox_collapse)
        return checkBox.isChecked
    }
}
