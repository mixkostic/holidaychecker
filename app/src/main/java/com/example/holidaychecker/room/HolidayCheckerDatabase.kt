package com.example.holidaychecker.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.holidaychecker.models.country.Country
import com.example.holidaychecker.models.holiday.Holiday

@Database(entities = [Country::class, Holiday::class], version = 1)
abstract class HolidayCheckerDatabase : RoomDatabase() {
    abstract fun countryDao(): CountriesDao
    abstract fun holidayDao(): HolidaysDao

    companion object {
        @Volatile
        private var INSTANCE: HolidayCheckerDatabase? = null

        fun getDatabase(context: Context): HolidayCheckerDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    HolidayCheckerDatabase::class.java,
                    "holiday_checker_db"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}