package com.example.holidaychecker.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.holidaychecker.models.country.Country

@Dao
interface CountriesDao {
    @Query("SELECT * from country")
    fun getAllCountries(): List<Country>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCountries(countries: List<Country>)

}