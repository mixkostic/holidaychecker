package com.example.holidaychecker.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.holidaychecker.models.holiday.Holiday

@Dao
interface HolidaysDao {
    @Query("SELECT * from holiday WHERE country LIKE :countryString")
    fun getHolidaysForCountry(countryString: String): List<Holiday>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertHolidays(holidays: List<Holiday>)
}