package com.example.holidaychecker.repository

import android.os.AsyncTask
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.holidaychecker.room.CountriesDao
import com.example.holidaychecker.models.country.CountriesData
import com.example.holidaychecker.models.country.Country
import com.example.holidaychecker.rest.ApiUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CountriesRepository(val countriesDao: CountriesDao) {
    private val holidayApi = ApiUtils.getHolidayApiService()

    fun getCountries(): LiveData<List<Country>> {
        val countriesLiveData = MutableLiveData<List<Country>>()

        holidayApi.getCountries().enqueue(object : Callback<CountriesData>{
            override fun onFailure(call: Call<CountriesData>, t: Throwable) {
                AsyncTask.execute { countriesLiveData.postValue(countriesDao.getAllCountries()) }
            }

            override fun onResponse(call: Call<CountriesData>, response: Response<CountriesData>) {
                val countries = response.body()?.countries
                countries?.let {
                    insertCountries(countries)
                }

                countriesLiveData.postValue(countries ?: emptyList())
            }

        })

        return countriesLiveData

    }

    private fun insertCountries(countries: List<Country>) {
        AsyncTask.execute { countriesDao.insertCountries(countries) }
    }
}