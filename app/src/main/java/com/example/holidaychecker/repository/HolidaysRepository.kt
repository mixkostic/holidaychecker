package com.example.holidaychecker.repository

import android.os.AsyncTask
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.holidaychecker.models.holiday.Holiday
import com.example.holidaychecker.models.holiday.HolidaysData
import com.example.holidaychecker.rest.ApiUtils
import com.example.holidaychecker.room.HolidaysDao
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HolidaysRepository(private val holidayDao: HolidaysDao) {
    private val holidayApi = ApiUtils.getHolidayApiService()
    private val year = 2018

    fun getHolidays(country: String): LiveData<List<Holiday>> {
        val holidaysLiveData = MutableLiveData<List<Holiday>>()

        holidayApi.getHolidays(country, year).enqueue(object : Callback<HolidaysData> {
            override fun onFailure(call: Call<HolidaysData>, t: Throwable) {
                AsyncTask.execute { holidaysLiveData.postValue(holidayDao.getHolidaysForCountry(country)) }
            }

            override fun onResponse(call: Call<HolidaysData>, response: Response<HolidaysData>) {
                val holidays = response.body()?.holidays
                holidays?.let {
                    AsyncTask.execute { holidayDao.insertHolidays(it) }
                }

                holidaysLiveData.postValue(holidays ?: emptyList())
            }

        })

        return holidaysLiveData
    }
}