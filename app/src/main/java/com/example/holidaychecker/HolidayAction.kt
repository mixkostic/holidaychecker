package com.example.holidaychecker

enum class HolidayAction(val action: String) {
    COMMON_PUBLIC("common_public"), AminusB("a_minus_b"), BminusA("b_minus_a");
}