package com.example.holidaychecker.rest

class ApiUtils {
    companion object {
        const val KEY_QUERY_PARAMETER_NAME = "key"
        const val KEY_VALUE = "eb9777c4-78fb-4132-901f-5a3b2d4981b0"

        fun getHolidayApiService(): HolidayApi {
            return RetrofitClient().getRetrofitInstance().create(HolidayApi::class.java)
        }
    }
}