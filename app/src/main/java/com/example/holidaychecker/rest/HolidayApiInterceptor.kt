package com.example.holidaychecker.rest

import okhttp3.Interceptor
import okhttp3.Response


class HolidayApiInterceptor: Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val originalHttpUrl = originalRequest.url()

        val url = originalHttpUrl.newBuilder()
            .addQueryParameter(ApiUtils.KEY_QUERY_PARAMETER_NAME, ApiUtils.KEY_VALUE)
            .build()

        val requestBuilder = originalRequest.newBuilder()
            .url(url)

        val request = requestBuilder.build()
        return chain.proceed(request)
    }
}