package com.example.holidaychecker.rest

import com.example.holidaychecker.models.country.CountriesData
import com.example.holidaychecker.models.holiday.HolidaysData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface HolidayApi {

    @GET("countries")
    fun getCountries(): Call<CountriesData>

    @GET("holidays")
    fun getHolidays(@Query("country") country: String, @Query("year") year: Int): Call<HolidaysData>

}