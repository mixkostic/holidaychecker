package com.example.holidaychecker

import android.text.format.DateUtils
import com.example.holidaychecker.models.holiday.Holiday
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class HolidayChecker {
    fun commonPublicHolidays(holidayList1: List<Holiday>, holidayList2: List<Holiday>): List<Holiday> {
        val publicHolidays1 = getPublicHolidays(holidayList1)
        val publicHolidays2 = getPublicHolidays(holidayList2)

        return publicHolidays1.filter { holiday ->  publicHolidays2.any { it.date == holiday.date } }
    }

    fun distinctHolidays(holidayList1: List<Holiday>, holidayList2: List<Holiday>): List<Holiday> {
        return holidayList1.filterNot { holiday ->  holidayList2.any { it.date == holiday.date } }
    }

    private fun getPublicHolidays(holidayList: List<Holiday>): List<Holiday> {
        return holidayList.filter { it.publicHoliday }
    }

    fun collapseContiguousHolidays(holidayList: List<Holiday>): List<Holiday> {
        val resultingList = mutableListOf<Holiday>()
        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.US)

        holidayList.forEachIndexed { index, holiday ->
            if (index > 0) {
                val previousHoliday = holidayList[index - 1]
                try {
                    val date = sdf.parse(holiday.date)
                    val previousHolidayDate = sdf.parse(previousHoliday.date)
                    if (date != null && previousHolidayDate != null) {
                        if (date.time - previousHolidayDate.time > DateUtils.DAY_IN_MILLIS)
                            resultingList.add(holiday)
                    }
                } catch (e: ParseException) {
                    print("HolidayChecker date parsing exception")
                }
            } else resultingList.add(holiday)

        }

        return resultingList
    }
}