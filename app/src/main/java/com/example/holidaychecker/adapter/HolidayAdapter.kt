package com.example.holidaychecker.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.holidaychecker.R
import com.example.holidaychecker.models.holiday.Holiday

class HolidayAdapter(private val holidayList: List<Holiday>): RecyclerView.Adapter<HolidayAdapter.HolidayHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolidayHolder {
        return HolidayHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.holiday_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return holidayList.size
    }

    override fun onBindViewHolder(holder: HolidayHolder, position: Int) {
        holder.textViewName.text = holidayList[position].name
        holder.textViewDate.text = holidayList[position].date
    }

    class HolidayHolder(v: View) : RecyclerView.ViewHolder(v) {
        val textViewName: TextView = v.findViewById(R.id.textview_holiday_name)
        val textViewDate: TextView = v.findViewById(R.id.textview_holiday_date)
    }

}