package com.example.holidaychecker.activity


import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import com.example.holidaychecker.R
import org.hamcrest.Matchers.anything
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@LargeTest
class CountriesActivityTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(CountriesActivity::class.java)

    @Before
    fun setup() {
        Intents.init()
    }

    @After
    fun release() {
        Intents.release()
    }

    @Test
    fun countriesActivityTest_clickOnCommon() {
        Thread.sleep(2000)
        onView(withId(R.id.button_common)).perform(click())

        Thread.sleep(1000)

        intended(hasComponent(HolidaysActivity::class.java.name))

    }


    @Test
    fun countriesActivityTest_SelectCountry() {
        Thread.sleep(2000)
        onView(withId(R.id.spinner_countryA)).perform(click())
        onData(anything()).atPosition(1).perform(click())

        Thread.sleep(500)
        onView(withId(R.id.button_b_a)).perform(click())
        Thread.sleep(1000)

        intended(hasComponent(HolidaysActivity::class.java.name))

    }

    @Test
    fun countriesActivityTest_minusOperation() {
        Thread.sleep(2000)
        onView(withId(R.id.spinner_countryA)).perform(click())
        onData(anything()).atPosition(1).perform(click())

        Thread.sleep(500)
        onView(withId(R.id.button_b_a)).perform(click())
        Thread.sleep(1000)

        intended(hasComponent(HolidaysActivity::class.java.name))

        onView(withId(R.id.recyclerView_holidays)).check(matches(isDisplayed()))

    }
}
