# HolidayChecker
HappyCouples AG aims to help people in long-distance relationships spend time together. For this, they had the idea to show couples when they can meet on public holidays. Like any lean startup, HappyCouples AG wishes to first test if people are interested in this service.

Your task is to build an MVP of a mobile app that
- Takes as input 2 countries: country A and country B
- Outputs
	- The list of public holidays common to both country A and country B
	- The list of holidays that are only in country A, but not in country B
	- The list of holidays that are only in country B, but not country A
	- Bonus features: collapse contiguous holidays, e.g., if April 1 and 2 are both holidays, report it as a single holiday
- Handles unexpected situations, e.g., no internet connection

Hints
	- You can use https://holidayapi.com to find out holidays in a country. The results can be limited to the year 2018.
	- This is an MVP, which means that the app will be used for user testing. On the one hand, this means you do not need to build the most scalable app, ready to go on the Store. On the other hand, the prototype will be changed according to customer feedback, and the changes need to happen fast.
	- The UI / UX design is left to your choice


